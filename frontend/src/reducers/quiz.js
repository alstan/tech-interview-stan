import { createAction, handleActions, combineActions } from 'redux-actions';

/**
 * @author Stanley Albuquerque
 * Utilização do redux action para minimizar o boilerplate code, não sendo necessário a 
 * criação de longos e verbosos blocos de switch case
 */

/**
 * Definição do estado inicial para o reducer quiz
 */
const initialState = {
    quizzes: [],
    quiz: {},
    isLoading: false
}

export const SHOW_ALL = 'quiz/SHOW_ALL';
export const SHOW = 'quiz/SHOW';
export const FETCH_ALL = 'quiz/FETCH_ALL';
export const FETCH = 'quiz/FETCH';
export const SAVE = 'quiz/SAVE';
export const REQUEST_FAILURE = 'quiz/REQUEST_FAILURE'

export const showAll = createAction(SHOW_ALL);
export const show = createAction(SHOW);
export const fetchAll = createAction(FETCH_ALL);
export const fetch = createAction(FETCH);
export const save = createAction(SAVE);
export const requestFailure = createAction(REQUEST_FAILURE);

const quiz = handleActions({
    [showAll]: (state, {payload}) => ({...state, quizzes: payload, isLoading: false}),
    [show]: (state, {payload}) => ({...state, quiz: payload, isLoading: false}),
    [combineActions(fetchAll, fetch, save)]: state => ({...state, isLoading: true}),  
}, initialState);

export default quiz;