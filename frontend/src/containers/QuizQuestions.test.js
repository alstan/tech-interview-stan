import React from 'react';
import { Provider } from 'react-redux';
import Enzyme from 'enzyme';
import { render, shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store'
import sinon from 'sinon';

import QuizQuestions from './QuizQuestions';
import { userQuiz } from '../mocks/userQuiz';

Enzyme.configure({ adapter: new Adapter() });

describe('QuizQuestions', () => {

    const initialState = {
        quiz: { 
            isLoading: false,
            quiz: userQuiz
        }
    };

    const mockStore = configureStore();
    let store, container;

    beforeEach(()=>{
        store = mockStore(initialState);
        container = render(<Provider store={store}><QuizQuestions params={{id: 1}}/></Provider> );
        //container.setProps({ params: {id: 1} });
    });

    it('renders the smart component', () => {
        expect(container.length).toEqual(1);
    });

    it('start the quiz', () => {
        const onButtonClick = sinon.spy();
        const quizListContainer = container.find('.start-quiz');
        //quizListContainer.simulate('click');
        expect(quizListContainer.length).toBeGreaterThan(0);
    });
  
});

