const express = require('express');
const path = require('path');
const _ = require('lodash');
var cache = require('memory-cache');
const app = express();

/**
 * Este trecho é usado exclusivamente para gerenciar os dados em cache do Node.
 * Foi preferivel o uso disto ao invéz do localStorage, assim o frontend pode ser reiniciado
 * várias vezes sem perda de dados.
 */

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());

/**
 * Onde será armazenado os dados da aplicação.
 */
cache.put('quizzes', []);

/**
 * Nos próximos trechos a seguir, os recursos são mapeados.
 */

app.get('/myquiz/*',  (req, res) => res.sendFile(path.join(__dirname, 'public', 'index.html')));

app.get('/server/quiz/', (req, res) => res.send(JSON.stringify(cache.get('quizzes'))));

app.get('/server/quiz/:id', (req, res) => res.send(_.find(cache.get('quizzes'), {id: req.params.id})));

app.post('/server/quiz/', (req, res) => {
    let quizzes = cache.get('quizzes');
    quizzes.push(req.body);
    cache.put('quizzes', quizzes)
    res.send(JSON.stringify(req.body));
});

app.listen(process.env.PORT || 8081);