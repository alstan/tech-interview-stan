import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetch } from '../reducers/quiz'
import QuizQuestions from '../components/QuizQuestions';

const mapStateToProps = state => ({
    isLoading: state.quiz.isLoading,
    quiz: state.quiz.quiz
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetch
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuizQuestions);