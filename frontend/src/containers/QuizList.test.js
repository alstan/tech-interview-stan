import React from 'react';
import Enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import renderer from 'react-test-renderer'
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import { showAll, fetchAll} from '../reducers/quiz';
import QuizList from './QuizList';

Enzyme.configure({ adapter: new Adapter() });

describe('QuizList', () => {

    const initialState = {
        quiz: { 
            quizzes: [
                {id: 1, title: 'Quiz 1', description: 'Quiz description 1'},
                {id: 2, title: 'Quiz 2', description: 'Quiz description 2'},
            ] 
        }
    };

    const mockStore = configureStore();
    let store, container;

    beforeEach(()=>{
        store = mockStore(initialState);
        container = render(<QuizList store={store} /> )  ;
    });

    it('renders the smart component', () => {
        expect(container.length).toEqual(1);
    });

    it('renders the list of quiz', () => {
        const quizListContainer = container.find('.quiz-row-container');
        expect(quizListContainer.children().length).toEqual(initialState.quiz.quizzes.length);
    })
  
});