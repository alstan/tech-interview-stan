import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import quiz from './quiz';

const rootReducer = combineReducers({
    quiz,
    routing: routerReducer
});

export default rootReducer;