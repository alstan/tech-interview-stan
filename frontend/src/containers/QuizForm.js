
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { save } from '../reducers/quiz'
import QuizForm from '../components/QuizForm';

const mapStateToProps = state => ({
    isLoading: state.quiz.isLoading
})

const mapDispatchToProps = dispatch => bindActionCreators({
    save
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuizForm);