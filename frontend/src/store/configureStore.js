import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../reducers'
import rootSaga from './rootSaga';

/**
 * Configuração do Store e do middleware Saga
 */

const sagaMiddleware = createSagaMiddleware();

export const configureStore = () => {

    const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

    store.runSaga = sagaMiddleware.run;
    store.runSaga(rootSaga);

    return store;
}  