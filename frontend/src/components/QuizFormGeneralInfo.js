import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, TextArea, Card } from 'semantic-ui-react';

/**
 * @author Stanley Albuquerque
 * @param {Object} props 
 * Este código foi separado em um componete para daixar o código do componente QuizForm limpo
 * e fácil de manter. 
 */
const QuizFormQuestions = (props) => {
    return (
        <div>
            {props.visible && 
                <Form onSubmit={props.onSubmit}>
                    <Card>
                        <Card.Content header='General Info' />
                        <Card.Content>
                            <Form.Field>
                                <label>Title</label>
                                <input name='title' placeholder='Title' maxLength='100' required/>
                            </Form.Field>
                            <Form.Field>
                                <label>Description</label>
                                <TextArea name='description' placeholder='Description' maxLength='200' required/>
                            </Form.Field>
                        </Card.Content>
                    </Card>
                    <Button type='submit' color='green'>Next Step</Button>
                </Form>
            }
        </div>
    )
}

QuizFormQuestions.propTypes = {
    visible: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired
}

export default QuizFormQuestions;