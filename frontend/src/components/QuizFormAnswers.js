import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Grid } from 'semantic-ui-react';

/**
 * @author Stanley Albuquerque
 * @param {Object} props 
 * Este código foi separado em um componete para daixar o código do componente QuizForm limpo
 * e fácil de manter. 
 */
const QuizFormAnswers= (props) => {
    return (
        <div>
            {props.currentAnswers.map(answer => 
                <div key={answer}>
                    <Grid>
                        <Grid.Column computer={12}>
                            <Form.Field>
                                <input name='answers' placeholder={'Answer ' + answer} maxLength='100' required/>
                            </Form.Field>
                        </Grid.Column>
                        <Grid.Column computer={4}>
                            <Form.Field>
                                <input type='radio' name='results' required/><span style={{marginLeft: '10px'}}> Correct</span>
                            </Form.Field>
                        </Grid.Column>
                    </Grid>
                </div>
            )}
            {props.currentAnswers.length < 8 &&
                <Button type='button' size='mini' style={{marginTop: '20px'}} onClick={props.onIncreaseAnswers}>Add Another Answer</Button>
            }
        </div>
    )
}

QuizFormAnswers.propTypes = {
    currentAnswers: PropTypes.array.isRequired,
    onIncreaseAnswers: PropTypes.func.isRequired
}

export default QuizFormAnswers;