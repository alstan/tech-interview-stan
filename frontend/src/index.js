import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom'; 
import { configureStore } from './store/configureStore'
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import routes from './routes';


const store = configureStore();
const history = syncHistoryWithStore(browserHistory, store);

function run() {
    ReactDOM.render(
        <Provider store={store}>
            <Router history={history}>
                {routes}
            </Router>
        </Provider>
        , document.getElementById('root')
    );
}

function init(){
    //localStorage.setItem('storage', JSON.stringify({quizzes:[]}));
    run();
    store.subscribe(run);
}

init();





