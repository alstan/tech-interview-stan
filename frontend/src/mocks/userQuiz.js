export const userQuiz = {
    id: "a86676f0-9334-11e8-b0a5-f12d74ef428e",
    title: "teste 1",
    description: "description 1",
    questions: [
        {
            id: "a1f6f920-9334-11e8-b0a5-f12d74ef428e",
            question: "teste question 1",
            answers: [
                {
                    id: "a1f6f921-9334-11e8-b0a5-f12d74ef428e",
                    answer: "gasdg 1",
                    correct: true
                },
                {
                    id: "a1f6f922-9334-11e8-b0a5-f12d74ef428e",
                    answer: "jjkl",
                    correct: false
                },
                {
                    id: "a1f6f923-9334-11e8-b0a5-f12d74ef428e",
                    answer: "mdffdg",
                    correct: false
                },
                {
                    id: "a1f6f924-9334-11e8-b0a5-f12d74ef428e",
                    answer: "dfgdfgdfg",
                    correct: false
                }
            ]
        },
        {
            id: "a8664fe0-9334-11e8-b0a5-f12d74ef428e",
            question: "dsfsdfsdf 2",
            answers: [
                {
                    id: "a8664fe1-9334-11e8-b0a5-f12d74ef428e",
                    answer: "dfssdfsd",
                    correct: false
                },
                {
                    id: "a8664fe2-9334-11e8-b0a5-f12d74ef428e",
                    answer: "sdfsdfs",
                    correct: false
                },
                {
                    id: "a8664fe3-9334-11e8-b0a5-f12d74ef428e",
                    answer: "sdfsdfsdf",
                    correct: true
                },
                {
                    id: "a8664fe4-9334-11e8-b0a5-f12d74ef428e",
                    answer: "sdfsdfsdf",
                    correct: false
                }
            ]
        }
    ]
}