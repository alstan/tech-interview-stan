import React from 'react';
import PropTypes from 'prop-types';
import { Grid, Card, Button, Dimmer, Loader } from 'semantic-ui-react';
import { Link } from 'react-router';

/**
 * @class 
 * @author Stanley Albuquerque
 * @class QuizList, componente que irá mostrar a lista de quizzes criados pelo usuário
 */
class QuizList extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.fetchAll();
    }

    render() {
        return (
            <div className='quiz-list'>
                <Dimmer active={this.props.isLoading} inverted>
                    <Loader size='medium'>Loading</Loader>
                </Dimmer>
                
                <Link to="/myquiz/new"><Button size='large' basic color='blue'>Create a new quiz</Button></Link>
                
                <Grid columns={3} style={{marginTop: '20px'}}>
                    <Grid.Row className='quiz-row-container'>
                        {this.props.quizzes.map(quiz => 
                            <Grid.Column key={quiz.id}>
                                <Card
                                    href={`/myquiz/quiz/${quiz.id}`}
                                    header={quiz.title}
                                    description={quiz.description}
                                />
                            </Grid.Column>
                        )}
                    </Grid.Row>
                </Grid>
            </div>
        );
    }

}

QuizList.propTypes = {
    quizzes: PropTypes.array.isRequired
}

export default QuizList;