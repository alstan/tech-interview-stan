import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchAll } from '../reducers/quiz'
import QuizList from '../components/QuizList';

const mapStateToProps = state => ({
    quizzes: state.quiz.quizzes
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchAll
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuizList);