//var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');
var debug = process.argv.indexOf('-p') === -1;
var path = require('path');

var config = {
    context: path.join(__dirname, "src"),
    devtool: 'cheap-module-source-map',
    entry: "./index.js",
    output: {
        path: path.join(__dirname, "public/resources/js/"),
        filename: "bundle.js"
    },
    plugins: debug ? [ ] : [
    	new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        }),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin(
            {
                sourceMap: true,
                minimize: true,
                mangle: true,
                compress: {
                    warnings: false, 
                    pure_getters: true,
                    unsafe: true,
                    unsafe_comps: true,
                    screw_ie8: true
                },
                output: {
                    comments: false,
                },
                exclude: [/\.min\.js$/gi] 
            }
        ),
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new webpack.NoErrorsPlugin()
    ],
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-2', 'stage-3' ]
                }
            }
        ]
    }
};


module.exports = config;

