import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, TextArea, Card } from 'semantic-ui-react';

/**
 * @author Stanley Albuquerque
 * @param {Object} props 
 * Este código foi separado em um componete para daixar o código do componente QuizForm limpo
 * e fácil de manter. 
 */
const QuizFormQuestions= (props) => {
    return (
        <div>
            {props.visible && 
                <Form onSubmit={props.onSubmit}>
                    <Card>
                        <Card.Content>
                            <Form.Field>
                                <label>Question {props.questions.length + 1}</label>
                                <TextArea name='question' maxLength='120' required/>
                            </Form.Field>

                            {props.children}

                        </Card.Content>
                    </Card>
                    
                    {
                        props.questions.length < 10 &&
                        <Button type='submit' color='green'>Add Another Question</Button>
                    }
                     
                    <Button color='blue' type='submit' onClick={props.onFinish}>Finish Quiz</Button>
                </Form>
            }
        </div>
    )
}

QuizFormQuestions.propTypes = {
    visible: PropTypes.bool.isRequired,
    onSubmit: PropTypes.func.isRequired,
    onFinish: PropTypes.func.isRequired,
    questions: PropTypes.array.isRequired
}

export default QuizFormQuestions;