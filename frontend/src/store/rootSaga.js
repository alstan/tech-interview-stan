import { takeLatest, all } from 'redux-saga/effects';

import * as Quiz from '../sagas/quiz';
import * as Types from "../reducers/quiz";
import Api from '../services/Api';

/**
 * API utilizada para facilitar o uso de requisições
 */
const api = Api.create();

/**
 * REgistrando os Sagas
 */
export default function * root() {
    yield all([
        takeLatest(Types.FETCH_ALL, Quiz.getQuizzes, api),
        takeLatest(Types.FETCH, Quiz.getQuiz, api),
        takeLatest(Types.SAVE, Quiz.saveQuiz, api),
    ])
}