import React from 'react';
import Enzyme from 'enzyme';
import { mount } from 'enzyme';
import Root from './Root';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

describe('mountedRoot', () => {
    let mountedRoot;
    const root = () => {
        if (!mountedRoot) {
            mountedRoot = mount(
            <Root />
        );
        }
        return mountedRoot;
    }

    it('renders a div that wrap all components', () => {
        const divs = root().find('div');
        expect(divs.length).toBeGreaterThan(0);
    });
  
});