
# Considerações sobre o projeto

Foi adotado alguns padrões que são difundidos na comunidade, como não adotar nomes para os arquivos como por exemplo fooReducer.js ou fooContainer.js, 
em vez disso foi utilizado padrões como reducers/foo.js ou containers/foo.js, os arquivos de teste ficam na mesma pasta de seu componente, existem
vários artigos na internet onde é encorajado essa prática. Um outro ponto interessante é um padrão chamado Duck, onde os action types, action creators
e reducers devem permanecer no mesmo arquivo, separar estes trechos em diferentes arquivos é uma prática questionável. Por último e não menos importante,
como o projeto é bastante pequeno e possui apenas um domínio que é o "quiz", não foi feito a separação por domínio, em uma aplicação maior é fortemente
recomendado a pratica de separar as pastas de reducers, components, containes, etc, em seus respectivos domínios

## Ambiente necessário

- NodeJs 8+;
- NPM 5+

## Executar o projeto

No terminal, dirija-se a pasta /frontend e execute os seguintes comandos:

- npm install
- node server.js
- npm run build

O projeto estará disponível em http://localhost:8081/myquiz/home

Para executar os testes utilize:

- npm run test
