import React from 'react';
import { Route } from 'react-router';

import Root from './components/Root';
import QuizList from './containers/QuizList';
import QuizForm from './containers/QuizForm';
import QuizQuestions from './containers/QuizQuestions';

const routes = (
    <Route path='/myquiz' component={Root}>
        <Route path='home' component={QuizList} />
        <Route path='new' component={QuizForm} />
        <Route path='quiz/:id' component={QuizQuestions} />
    </Route>
);

export default routes;        