import React from 'react';
import { Header } from 'semantic-ui-react';

const Root = (props) => {
    return (
        <div>
            <header className="main-header">
                <Header as='h1'>My Quiz</Header>
            </header>
            <div className="content-main">
               {props.children}
            </div>
        </div>
    )
}

export default Root;