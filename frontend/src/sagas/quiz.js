import { call, put } from 'redux-saga/effects';
import * as Actions from '../reducers/quiz';

export function * getQuizzes(api, { payload }) {
    try{
        const response = yield call(api.get, '/quiz/');
        yield put(Actions.showAll(response.data));
    }
    catch(error) {
        yield put(Actions.requestFailure(error));
    }
}

export function * getQuiz(api, { payload }) {
    try{
        const response = yield call(api.get, `/quiz/${payload}`);
        yield put(Actions.show(response.data));
    }
    catch(error) {
        yield put(Actions.requestFailure(error));
    }
}

export function * saveQuiz(api, { payload }) {
    try{
        yield call(api.post, '/quiz/', payload);
        yield put(Actions.show(payload));
    }
    catch(error) {
        yield put(Actions.requestFailure(error));
    }
}