import apisauce from 'apisauce';

const create = (baseURL = '/server') => {
    return apisauce.create({ baseURL });
}

export default { create };