import React from 'react';
import PropTypes from 'prop-types';
import { Button, Form, Grid, Card, Radio, Dimmer, Loader } from 'semantic-ui-react';
import { Link } from 'react-router';
import _ from 'lodash';

/**
 * @author Stanley Albuquerque
 * Este componente mostrará as informações gerais e as perguntas do quiz selecionado.
 * O usuário poderá responder a todas as perguntas.
 */

class QuizQuestions extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            started: false,
            currentQuestion: null,
            wrongQuestions: 0
        }
        this.questionGen = this.getNextQuestion();
    }

    componentDidMount() {
        const { id } = this.props.params;
        this.props.fetch(id);
        this.setState({quiz: id})
    }

    /**
     * @method getNextQuestion é invocado a cada vez que o usuário avança para a proxima pergunta
     * para evitar loops em meio a marcação, este function generator irá trazer o próximo item a
     * cada vez que for invocado.
     */
    * getNextQuestion() {
        yield* this.props.quiz.questions.map(question => question); 
    }

    nextQuestion() {
        this.setState({currentQuestion: this.questionGen.next().value})
    }

    /**
     * @method chooseAnswer invocado cada vez que o usuário escolhe uma resposta, é recebido
     * @param {Object} answer que é a pergunta escolhido junto com o @param {Number} index que será usado para 
     * indicar qual a posição do objeto no array de answer.
     */
    chooseAnswer(answer, index) {
        const { answers, currentQuestion } = this.state.currentQuestion;
        let newAnswers = [...answers];//criando uma copia do array para não mutar o array original

        const correctAnswer = _.find(answers, {correct: true}) //encontrar a resposta correta
        const indexCorrect = _.findIndex(answers, {correct: true});//encontrar o index da resposta correta
        newAnswers[indexCorrect] = {...correctAnswer, color: 'green'}

        if(answer.id !== correctAnswer.id){//se caso o usuario escolher um pergunta incorreta, a pergunta escolhida sera marcada de vermelho
            newAnswers[index] = {...answer, color: 'red'}
            this.setState({wrongQuestions: this.state.wrongQuestions + 1});
        }
       
        this.setState({currentQuestion: {...currentQuestion, answers: newAnswers, answered: true}});
    }

    startQuiz() {
        this.setState({started: true})
        this.nextQuestion();
    }

    render() {
        const { quiz } = this.props;
        const defaultStyle = {marginTop: '20px', textAlign: 'center'};

        return(
            <div>
                <Dimmer active={this.props.isLoading} inverted>
                    <Loader size='medium'>Loading</Loader>
                </Dimmer>
                <Grid>
                    <Grid.Column computer={8} tablet={12} mobile={16}>
                        {quiz.title &&
                            <div>
                                {!this.state.started &&
                                    <Card>
                                        <Card.Content header={quiz.title} />
                                        <Card.Content>
                                            {quiz.description}
                                        </Card.Content>
                                        <Card.Content>
                                            <button  className='start-quiz' onClick={() => this.startQuiz()}>Start</button>
                                        </Card.Content>
                                    </Card>
                                }
                                {(this.state.started && this.state.currentQuestion) &&
                                    <Card>
                                        <Card.Content header={'Question 1'} />
                                        <Card.Content>
                                            <h4>{this.state.currentQuestion.question}</h4>
                                            {this.state.currentQuestion.answers.map((o, i) => 
                                                <Form.Field key={i}>

                                                    <Button 
                                                        fluid 
                                                        onClick={() => this.chooseAnswer(o, i)}
                                                        color={o.color}
                                                        disabled={this.state.currentQuestion.answered}>{o.answer}</Button>
                                                </Form.Field>
                                            )}
                                        </Card.Content>
                                        <Card.Content>
                                            <Button 
                                                color='blue' 
                                                onClick={() => this.nextQuestion()} 
                                                disabled={!this.state.currentQuestion.answered}>Next</Button>
                                        </Card.Content>
                                    </Card>
                                }

                                {(!this.state.currentQuestion && this.state.started) &&
                                <span>
                                    <h3>You finished the quiz.</h3>
                                    <p>You score is {this.props.quiz.questions.length - this.state.wrongQuestions} of {this.props.quiz.questions.length}.</p>
                                </span>
                                }
                                <Link to='/myquiz/home'>
                                    <Button style={defaultStyle}>Back to home</Button>
                                </Link>
                            </div>
                        }
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

QuizQuestions.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    quiz: PropTypes.object.isRequired
}

export default QuizQuestions;