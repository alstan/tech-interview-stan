import React from 'react';
import PropTypes from 'prop-types';
import { Button, Header, Grid, Modal, Dimmer, Loader } from 'semantic-ui-react';
import { Link } from 'react-router';
import uuidv1 from 'uuid/v1';

import QuizFormGeneralInfo from './QuizFormGeneralInfo';
import QuizFormQuestions from './QuizFormQuestions';
import QuizFormAnswers from './QuizFormAnswers';

/**
 * @class QuizForm encapsula alguns componentes os quais são usados pelo componente 
 * onde o usuário cria o quiz, em outras palavras, um unico componente foi dividado
 * em alguns outros para manter o código limpo e simples.
 */
class QuizForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = { 
            questions: [],
            currentAnswers: [1, 2, 3, 4],
            finished: false,
            quizSaved: false
        }
    }

    handleSubmitGeneral = event => {
        event.preventDefault();//evitar a ação padrão do evento submit

        this.setState({title: event.target.title.value, description: event.target.description.value});
    }

    /**
     * @method handleSubmitQuestion recebe @event submit onde será necessario impedir
     * a ação padrão de ser executada. Esse metodo será invocado a cada vez que uma nova questão é criada.
     */
    handleSubmitQuestion = event => {
        event.preventDefault();

        //questão montada a partir dos dados do formulário
        let question = {id: uuidv1(), question: event.target.question.value, answers: this.getAnswers(event.target)}
        let questions = [...this.state.questions, question];

        this.setState({questions});

        this.clearQuestionsAndAnswers(event.target);

        if(this.state.finished) {
            this.saveQuiz(questions);
            this.setState({quizSaved: true});
        }
    }

    handleIncreaseAnswers = () => {
        let { currentAnswers } = this.state;
        currentAnswers.push(currentAnswers.length + 1);
        this.setState({currentAnswers});
    }

    handleFinish = () => {
        this.setState({finished: true});
    }

    /**
     * @method getAnswers recebe @param form onde será extraido os valores dos campos
     * Necessário utilizar um loop for convencinal devido o objeto ser um RadioNodeList
     */
    getAnswers = (form) => {
        let answers = [];
        for(let i = 0; i < form.answers.length; i++){
            answers.push({id:uuidv1(), answer: form.answers[i].value, correct: form.results[i].checked});
        }

        return answers;
    }

    clearQuestionsAndAnswers = (form) => {
        form.reset();
        this.setState({currentAnswers: [1, 2, 3, 4]})
    }

    isGeneralInfoFilled = () => !!this.state.title;

    saveQuiz = (questions) => {
        const {title, description} = this.state;
        const quiz = {id: uuidv1(), title, description, questions};
        this.props.save(quiz);
    }

    render() {
        return (
            <div>
                <Dimmer active={this.props.isLoading} inverted>
                    <Loader size='medium'>Loading</Loader>
                </Dimmer>
                <Grid>
                    <Grid.Column computer={8} tablet={12} mobile={16}>
                        <Header as='h3'>New Quiz</Header>

                        <QuizFormGeneralInfo 
                            visible={!this.isGeneralInfoFilled()}
                            onSubmit={this.handleSubmitGeneral} />

                        <QuizFormQuestions 
                            visible={this.isGeneralInfoFilled()}
                            onSubmit={this.handleSubmitQuestion}
                            onAddQuestion={this.handleAddQuestions}
                            onFinish={this.handleFinish}
                            questions={this.state.questions}>
                            <QuizFormAnswers
                                onIncreaseAnswers={this.handleIncreaseAnswers} 
                                currentAnswers={this.state.currentAnswers}
                                />
                        </QuizFormQuestions>
                        
                        <Modal open={this.state.quizSaved}>
                            <Modal.Header>Success</Modal.Header>
                            <Modal.Content>
                                <p>You created a new quiz.</p>
                            </Modal.Content>
                            <Modal.Actions>
                                <Link to='/myquiz/home'>
                                    <Button size='large'>Back to home</Button>
                                </Link>
                            </Modal.Actions>
                        </Modal>

                        <Link to='/myquiz/home'>
                            <Button style={{marginTop: '20px'}}>Back to home</Button>
                        </Link>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }

}

QuizForm.propTypes = {
    isLoading: PropTypes.bool.isRequired
}

export default QuizForm;